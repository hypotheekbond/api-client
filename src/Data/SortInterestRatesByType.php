<?php

namespace Dnhb\ApiClient\Data;

use MyCLabs\Enum\Enum;

/**
 * Class SortInterestRatesByType
 */
final class SortInterestRatesByType extends Enum
{
    /** */
    const INTEREST_RATES_SORT_TYPE_PERCENTAGE = 'percentage';
}