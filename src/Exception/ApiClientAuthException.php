<?php
declare(strict_types = 1);

namespace Dnhb\ApiClient\Exception;

/**
 * Class ApiClientAuthException
 *
 * @package Dnhb\ApiClient\Exception
 */
final class ApiClientAuthException extends ApiClientException
{
}