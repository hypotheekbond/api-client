<?php
declare(strict_types = 1);

namespace Dnhb\ApiClient\Exception;

/**
 * Class InvalidArgumentException
 *
 * @package Dnhb\ApiClient\Exception
 */
final class ApiClientInvalidArgumentException extends ApiClientException
{
}