<?php
declare(strict_types = 1);

namespace Dnhb\ApiClient\Exception;

/**
 * Class ApiClientUnexpectedResultException
 *
 * @package Dnhb\ApiClient\Exception
 */
final class ApiClientUnexpectedResultException extends ApiClientException
{
}