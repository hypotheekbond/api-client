<?php
declare(strict_types = 1);

namespace Dnhb\ApiClient\Exception;

/**
 * Class ScopesNotMatchException
 */
final class ScopesNotMatchException extends ApiClientException
{
}