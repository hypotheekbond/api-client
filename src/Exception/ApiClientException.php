<?php
declare(strict_types = 1);

namespace Dnhb\ApiClient\Exception;

use Exception;

/**
 * Class ApiClientException
 *
 * @package Dnhb\ApiClient\Exception
 */
class ApiClientException extends Exception
{
}