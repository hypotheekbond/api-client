<?php
declare(strict_types = 1);

namespace Dnhb\ApiClient\Exception;

/**
 * Class ValueNotSetException
 *
 * @package Dnhb\ApiClient\Exception
 */
class ValueNotSetException extends ApiClientException
{
}