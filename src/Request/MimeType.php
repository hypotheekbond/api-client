<?php
declare(strict_types = 1);

namespace Dnhb\ApiClient\Request;

/**
 * Class MimeType
 *
 * @package Dnhb\ApiClient\Request
 */
abstract class MimeType
{
    /** */
    const JSON = 'application/json';

    /** */
    const XML = 'application/xml';
}