<?php
declare(strict_types = 1);

namespace Dnhb\ApiClient\Request;

/**
 * Class Method
 *
 * @package Dnhb\ApiClient\Request
 */
abstract class Method
{
    /** */
    const GET = 'GET';

    /** */
    const POST = 'POST';
}