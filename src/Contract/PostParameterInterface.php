<?php
declare(strict_types = 1);

namespace Dnhb\ApiClient\Contract;

/**
 * Interface PostParameterInterface
 *
 * @package Dnhb\ApiClient\Contract
 *
 * @deprecated
 * @see \Dnhb\ApiClient\Contract\JsonablePostParameter
 */
interface PostParameterInterface extends JsonablePostParameter
{
}
