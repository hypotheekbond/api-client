<?php
declare(strict_types = 1);

namespace Dnhb\ApiClient\Contract;

/**
 * Interface GetApiInterface
 *
 * @package Dnhb\ApiClient\Contract
 */
interface GetApiRequestInterface extends ApiRequestInterface
{
}